class SaveLoadPanel extends MovieClip
{
   static var SCREENSHOT_DELAY = 750;
   function SaveLoadPanel()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this.SaveLoadList_mc = this.List_mc;
      this.bSaving = true;
   }
   function onLoad()
   {
      this.ScreenshotLoader = new MovieClipLoader();
      this.ScreenshotLoader.addListener(this);
      gfx.io.GameDelegate.addCallBack("ConfirmOKToLoad",this,"onOKToLoadConfirm");
      gfx.io.GameDelegate.addCallBack("onSaveLoadBatchComplete",this,"onSaveLoadBatchComplete");
      gfx.io.GameDelegate.addCallBack("ScreenshotReady",this,"ShowScreenshot");
      this.SaveLoadList_mc.addEventListener("itemPress",this,"onSaveLoadItemPress");
      this.SaveLoadList_mc.addEventListener("selectionChange",this,"onSaveLoadItemHighlight");
      this.iBatchSize = this.SaveLoadList_mc.maxEntries;
      this.PlayerInfoText.createTextField("LevelText",this.PlayerInfoText.getNextHighestDepth(),0,0,200,30);
      this.PlayerInfoText.LevelText.text = "$Level";
      this.PlayerInfoText.LevelText._visible = false;
   }
   function __get__isSaving()
   {
      return this.bSaving;
   }
   function __set__isSaving(abFlag)
   {
      this.bSaving = abFlag;
      return this.__get__isSaving();
   }
   function __get__selectedIndex()
   {
      return this.SaveLoadList_mc.__get__selectedIndex();
   }
   function __get__platform()
   {
      return this.iPlatform;
   }
   function __set__platform(aiPlatform)
   {
      this.iPlatform = aiPlatform;
      return this.__get__platform();
   }
   function __get__batchSize()
   {
      return this.iBatchSize;
   }
   function __get__numSaves()
   {
      return this.SaveLoadList_mc.__get__entryList().length;
   }
   function onSaveLoadItemPress(event)
   {
      if(!this.bSaving)
      {
         gfx.io.GameDelegate.call("IsOKtoLoad",[this.SaveLoadList_mc.__get__selectedIndex()]);
      }
      else
      {
         this.dispatchEvent({type:"saveGameSelected",index:this.SaveLoadList_mc.__get__selectedIndex()});
      }
   }
   function onOKToLoadConfirm()
   {
      this.dispatchEvent({type:"loadGameSelected",index:this.SaveLoadList_mc.__get__selectedIndex()});
   }
   function onSaveLoadItemHighlight(event)
   {
      if(this.iScreenshotTimerID != undefined)
      {
         clearInterval(this.iScreenshotTimerID);
         this.iScreenshotTimerID = undefined;
      }
      if(this.ScreenshotRect != undefined)
      {
         this.ScreenshotRect.removeMovieClip();
         this.PlayerInfoText.textField.SetText(" ");
         this.PlayerInfoText.DateText.SetText(" ");
         this.PlayerInfoText.PlayTimeText.SetText(" ");
         this.ScreenshotRect = undefined;
      }
      if(event.index != -1)
      {
         this.iScreenshotTimerID = setInterval(this,"PrepScreenshot",SaveLoadPanel.SCREENSHOT_DELAY);
      }
      this.dispatchEvent({type:"saveHighlighted",index:this.SaveLoadList_mc.__get__selectedIndex()});
   }
   function PrepScreenshot()
   {
      clearInterval(this.iScreenshotTimerID);
      this.iScreenshotTimerID = undefined;
      if(this.bSaving)
      {
         gfx.io.GameDelegate.call("PrepSaveGameScreenshot",[this.SaveLoadList_mc.__get__selectedIndex() - 1,this.SaveLoadList_mc.__get__selectedEntry()]);
      }
      else
      {
         gfx.io.GameDelegate.call("PrepSaveGameScreenshot",[this.SaveLoadList_mc.__get__selectedIndex(),this.SaveLoadList_mc.__get__selectedEntry()]);
      }
   }
   function ShowScreenshot()
   {
      this.ScreenshotRect = this.ScreenshotHolder.createEmptyMovieClip("ScreenshotRect",0);
      this.ScreenshotLoader.loadClip("img://BGSSaveLoadHeader_Screenshot",this.ScreenshotRect);
      if(this.SaveLoadList_mc.__get__selectedEntry().corrupt == true)
      {
         this.PlayerInfoText.textField.SetText("$SAVE CORRUPT");
      }
      else if(this.SaveLoadList_mc.__get__selectedEntry().obsolete == true)
      {
         this.PlayerInfoText.textField.SetText("$SAVE OBSOLETE");
      }
      else if(this.SaveLoadList_mc.__get__selectedEntry().name != undefined)
      {
         var _loc2_ = this.SaveLoadList_mc.__get__selectedEntry().name;
         var _loc3_ = 20;
         if(_loc2_.length > _loc3_)
         {
            _loc2_ = _loc2_.substr(0,_loc3_ - 3) + "...";
         }
         if(this.SaveLoadList_mc.__get__selectedEntry().raceName != undefined && this.SaveLoadList_mc.__get__selectedEntry().raceName.length > 0)
         {
            _loc2_ = _loc2_ + (", " + this.SaveLoadList_mc.__get__selectedEntry().raceName);
         }
         if(this.SaveLoadList_mc.__get__selectedEntry().level != undefined && this.SaveLoadList_mc.__get__selectedEntry().level > 0)
         {
            _loc2_ = _loc2_ + (", " + this.PlayerInfoText.LevelText.text + " " + this.SaveLoadList_mc.__get__selectedEntry().level);
         }
         this.PlayerInfoText.textField.textAutoSize = "shrink";
         this.PlayerInfoText.textField.SetText(_loc2_);
      }
      else
      {
         this.PlayerInfoText.textField.SetText(" ");
      }
      if(this.SaveLoadList_mc.__get__selectedEntry().playTime != undefined)
      {
         this.PlayerInfoText.PlayTimeText.SetText(this.SaveLoadList_mc.__get__selectedEntry().playTime);
      }
      else
      {
         this.PlayerInfoText.PlayTimeText.SetText(" ");
      }
      if(this.SaveLoadList_mc.__get__selectedEntry().dateString != undefined)
      {
         this.PlayerInfoText.DateText.SetText(this.SaveLoadList_mc.__get__selectedEntry().dateString);
      }
      else
      {
         this.PlayerInfoText.DateText.SetText(" ");
      }
   }
   function onLoadInit(aTargetClip)
   {
      aTargetClip._width = this.ScreenshotHolder.sizer._width;
      aTargetClip._height = this.ScreenshotHolder.sizer._height;
   }
   function onSaveLoadBatchComplete(abDoInitialUpdate)
   {
      var _loc2_ = 20;
      for(var _loc3_ in this.SaveLoadList_mc.__get__entryList())
      {
         if(this.SaveLoadList_mc.__get__entryList()[_loc3_].text.length > _loc2_)
         {
            this.SaveLoadList_mc.__get__entryList()[_loc3_].text = this.SaveLoadList_mc.__get__entryList()[_loc3_].text.substr(0,_loc2_ - 3) + "...";
         }
      }
      if(abDoInitialUpdate)
      {
         var _loc4_ = "$[NEW SAVE]";
         if(this.bSaving && this.SaveLoadList_mc.__get__entryList()[0].text != _loc4_)
         {
            var _loc5_ = {name:" ",playTime:" ",text:_loc4_};
            this.SaveLoadList_mc.__get__entryList().unshift(_loc5_);
         }
         else if(!this.bSaving && this.SaveLoadList_mc.__get__entryList()[0].text == _loc4_)
         {
            this.SaveLoadList_mc.__get__entryList().shift();
         }
      }
      this.SaveLoadList_mc.InvalidateData();
      if(abDoInitialUpdate)
      {
         if(this.iPlatform != 0)
         {
            if(this.SaveLoadList_mc.__get__selectedIndex() == 0)
            {
               this.onSaveLoadItemHighlight({index:0});
            }
            else
            {
               this.SaveLoadList_mc.__set__selectedIndex(0);
            }
         }
         this.dispatchEvent({type:"saveListPopulated"});
      }
   }
   function DeleteSelectedSave()
   {
      if(!this.bSaving || this.SaveLoadList_mc.__get__selectedIndex() != 0)
      {
         if(this.bSaving)
         {
            gfx.io.GameDelegate.call("DeleteSave",[this.SaveLoadList_mc.__get__selectedIndex() - 1]);
         }
         else
         {
            gfx.io.GameDelegate.call("DeleteSave",[this.SaveLoadList_mc.__get__selectedIndex()]);
         }
         this.SaveLoadList_mc.__get__entryList().splice(this.SaveLoadList_mc.__get__selectedIndex(),1);
         this.SaveLoadList_mc.InvalidateData();
         this.onSaveLoadItemHighlight({index:this.SaveLoadList_mc.__get__selectedIndex()});
      }
   }
}
