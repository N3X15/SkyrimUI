class StartMenu extends MovieClip
{
   static var PRESS_START_STATE = "PressStart";
   static var MAIN_STATE = "Main";
   static var MAIN_CONFIRM_STATE = "MainConfirm";
   static var SAVE_LOAD_STATE = "SaveLoad";
   static var SAVE_LOAD_CONFIRM_STATE = "SaveLoadConfirm";
   static var DELETE_SAVE_CONFIRM_STATE = "DeleteSaveConfirm";
   static var DLC_STATE = "DLC";
   static var MARKETPLACE_CONFIRM_STATE = "MarketplaceConfirm";
   static var CONTINUE_INDEX = 0;
   static var NEW_INDEX = 1;
   static var LOAD_INDEX = 2;
   static var DLC_INDEX = 3;
   static var CREDITS_INDEX = 4;
   static var QUIT_INDEX = 5;
   function StartMenu()
   {
      super();
      this.MainList = this.MainListHolder.List_mc;
      this.SaveLoadListHolder = this.SaveLoadPanel_mc;
      this.DLCList_mc = this.DLCPanel.DLCList;
      this.DeleteSaveButton = this.DeleteButton;
      this.MarketplaceButton = this.DLCPanel.MarketplaceButton;
   }
   function InitExtensions()
   {
      Shared.GlobalFunc.SetLockFunction();
      this._parent.Lock("BR");
      this.Logo_mc.Lock("BL");
      this.Logo_mc._y = this.Logo_mc._y - 80;
      gfx.io.GameDelegate.addCallBack("sendMenuProperties",this,"setupMainMenu");
      gfx.io.GameDelegate.addCallBack("ConfirmNewGame",this,"ShowConfirmScreen");
      gfx.io.GameDelegate.addCallBack("ConfirmContinue",this,"ShowConfirmScreen");
      gfx.io.GameDelegate.addCallBack("FadeOutMenu",this,"DoFadeOutMenu");
      gfx.io.GameDelegate.addCallBack("FadeInMenu",this,"DoFadeInMenu");
      gfx.io.GameDelegate.addCallBack("onProfileChange",this,"onProfileChange");
      gfx.io.GameDelegate.addCallBack("StartLoadingDLC",this,"StartLoadingDLC");
      gfx.io.GameDelegate.addCallBack("DoneLoadingDLC",this,"DoneLoadingDLC");
      this.MainList.addEventListener("itemPress",this,"onMainButtonPress");
      this.MainList.addEventListener("listPress",this,"onMainListPress");
      this.MainList.addEventListener("listMovedUp",this,"onMainListMoveUp");
      this.MainList.addEventListener("listMovedDown",this,"onMainListMoveDown");
      this.MainList.addEventListener("selectionChange",this,"onMainListMouseSelectionChange");
      this.ButtonRect.handleInput = function()
      {
         return false;
      };
      this.ButtonRect.AcceptMouseButton.addEventListener("click",this,"onAcceptMousePress");
      this.ButtonRect.CancelMouseButton.addEventListener("click",this,"onCancelMousePress");
      this.ButtonRect.AcceptMouseButton.SetPlatform(0,false);
      this.ButtonRect.CancelMouseButton.SetPlatform(0,false);
      this.SaveLoadListHolder.addEventListener("loadGameSelected",this,"ConfirmLoadGame");
      this.SaveLoadListHolder.addEventListener("saveListPopulated",this,"OnSaveListOpenSuccess");
      this.SaveLoadListHolder.addEventListener("saveHighlighted",this,"onSaveHighlight");
      this.SaveLoadListHolder.List_mc.addEventListener("listPress",this,"onSaveLoadListPress");
      this.DeleteSaveButton._alpha = 50;
      this.MarketplaceButton._alpha = 50;
      this.DLCList_mc._visible = false;
   }
   function setupMainMenu()
   {
      var _loc7_ = 0;
      var _loc5_ = 1;
      var _loc6_ = 2;
      var _loc8_ = 3;
      var _loc9_ = 4;
      var _loc4_ = StartMenu.NEW_INDEX;
      if(this.MainList.__get__entryList().length > 0)
      {
         _loc4_ = this.MainList.__get__centeredEntry().index;
      }
      this.MainList.ClearList();
      if(arguments[_loc5_])
      {
         this.MainList.__get__entryList().push({text:"$CONTINUE",index:StartMenu.CONTINUE_INDEX,disabled:false});
         if(_loc4_ == StartMenu.NEW_INDEX)
         {
            _loc4_ = StartMenu.CONTINUE_INDEX;
         }
      }
      this.MainList.__get__entryList().push({text:"$NEW",index:StartMenu.NEW_INDEX,disabled:false});
      this.MainList.__get__entryList().push({text:"$LOAD",disabled:!arguments[_loc5_],index:StartMenu.LOAD_INDEX});
      if(arguments[_loc9_] == true)
      {
         this.MainList.__get__entryList().push({text:"$DOWNLOADABLE CONTENT",index:StartMenu.DLC_INDEX,disabled:false});
      }
      this.MainList.__get__entryList().push({text:"$CREDITS",index:StartMenu.CREDITS_INDEX,disabled:false});
      if(arguments[_loc7_])
      {
         this.MainList.__get__entryList().push({text:"$QUIT",index:StartMenu.QUIT_INDEX,disabled:false});
      }
      var _loc3_ = 0;
      while(_loc3_ < this.MainList.__get__entryList().length)
      {
         if(this.MainList.__get__entryList()[_loc3_].index == _loc4_)
         {
            this.MainList.RestoreScrollPosition(_loc3_,false);
         }
         _loc3_ = _loc3_ + 1;
      }
      this.MainList.InvalidateData();
      if(this.__get__currentState() == undefined)
      {
         if(arguments[_loc8_])
         {
            this.StartState(StartMenu.PRESS_START_STATE);
         }
         else
         {
            this.StartState(StartMenu.MAIN_STATE);
         }
      }
      else if(this.__get__currentState() == StartMenu.SAVE_LOAD_STATE || this.__get__currentState() == StartMenu.SAVE_LOAD_CONFIRM_STATE || this.__get__currentState() == StartMenu.DELETE_SAVE_CONFIRM_STATE)
      {
         this.ShowDeleteButtonHelp(false);
         this.StartState(StartMenu.MAIN_STATE);
      }
      if(arguments[_loc6_] != undefined)
      {
         this.VersionText.SetText("v " + arguments[_loc6_]);
      }
      else
      {
         this.VersionText.SetText(" ");
      }
   }
   function __get__currentState()
   {
      return this.strCurrentState;
   }
   function __set__currentState(strNewState)
   {
      if(strNewState == StartMenu.MAIN_STATE)
      {
         this.MainList.__set__disableSelection(false);
      }
      this.strCurrentState = strNewState;
      this.ChangeStateFocus(strNewState);
      return this.__get__currentState();
   }
   function handleInput(details, pathToFocus)
   {
      if(this.__get__currentState() == StartMenu.PRESS_START_STATE)
      {
         if(Shared.GlobalFunc.IsKeyPressed(details) && (details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_START || details.navEquivalent == gfx.ui.NavigationCode.ENTER))
         {
            this.EndState(StartMenu.PRESS_START_STATE);
         }
         gfx.io.GameDelegate.call("EndPressStartState",[]);
      }
      else if(pathToFocus.length > 0 && !pathToFocus[0].handleInput(details,pathToFocus.slice(1)))
      {
         if(Shared.GlobalFunc.IsKeyPressed(details))
         {
            if(details.navEquivalent == gfx.ui.NavigationCode.ENTER)
            {
               this.onAcceptPress();
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.TAB)
            {
               this.onCancelPress();
            }
            else if((details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_X || details.code == 88) && this.DeleteSaveButton._visible && this.DeleteSaveButton._alpha == 100)
            {
               this.ConfirmDeleteSave();
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_Y && this.__get__currentState() == StartMenu.DLC_STATE && this.MarketplaceButton._visible && this.MarketplaceButton._alpha == 100)
            {
               this.SaveLoadConfirmText.textField.SetText("$Open Xbox LIVE Marketplace?");
               this.SetPlatform(this.iPlatform);
               this.StartState(StartMenu.MARKETPLACE_CONFIRM_STATE);
            }
         }
      }
      return true;
   }
   function onAcceptPress()
   {
      switch(this.strCurrentState)
      {
         case StartMenu.MAIN_CONFIRM_STATE:
            if(this.MainList.__get__selectedEntry().index == StartMenu.NEW_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIStartNewGame"]);
               this.FadeOutAndCall("StartNewGame");
            }
            else if(this.MainList.__get__selectedEntry().index == StartMenu.CONTINUE_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               this.FadeOutAndCall("ContinueLastSavedGame");
            }
            else if(this.MainList.__get__selectedEntry().index == StartMenu.QUIT_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               gfx.io.GameDelegate.call("QuitToDesktop",[]);
            }
            break;
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
         case "SaveLoadConfirmStartAnim":
            gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
            this.FadeOutAndCall("LoadGame",[this.SaveLoadListHolder.__get__selectedIndex()]);
            break;
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
            this.SaveLoadListHolder.DeleteSelectedSave();
            if(this.SaveLoadListHolder.__get__numSaves() == 0)
            {
               this.ShowDeleteButtonHelp(false);
               this.StartState(StartMenu.MAIN_STATE);
               if(this.MainList.__get__entryList()[2].index == StartMenu.LOAD_INDEX)
               {
                  this.MainList.__get__entryList()[2].disabled = true;
               }
               if(this.MainList.__get__entryList()[0].index == StartMenu.CONTINUE_INDEX)
               {
                  this.MainList.__get__entryList().shift();
               }
               this.MainList.RestoreScrollPosition(1,true);
               this.MainList.InvalidateData();
            }
            else
            {
               this.EndState();
            }
            break;
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
            gfx.io.GameDelegate.call("OpenMarketplace",[]);
            this.StartState(StartMenu.MAIN_STATE);
      }
   }
   function isConfirming()
   {
      return this.strCurrentState == StartMenu.SAVE_LOAD_CONFIRM_STATE || this.strCurrentState == StartMenu.DELETE_SAVE_CONFIRM_STATE || this.strCurrentState == StartMenu.MARKETPLACE_CONFIRM_STATE || this.strCurrentState == StartMenu.MAIN_CONFIRM_STATE;
   }
   function onAcceptMousePress()
   {
      if(this.isConfirming())
      {
         this.onAcceptPress();
      }
   }
   function onCancelMousePress()
   {
      if(this.isConfirming())
      {
         this.onCancelPress();
      }
   }
   function onCancelPress()
   {
      switch(this.strCurrentState)
      {
         case StartMenu.MAIN_CONFIRM_STATE:
         case StartMenu.SAVE_LOAD_STATE:
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
         case StartMenu.DLC_STATE:
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            break;
         default:
      }
      gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
      this.EndState();
   }
   function onMainButtonPress(event)
   {
      if(this.strCurrentState == StartMenu.MAIN_STATE || this.iPlatform == 0)
      {
         switch(event.entry.index)
         {
            case StartMenu.CONTINUE_INDEX:
               gfx.io.GameDelegate.call("CONTINUE",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.NEW_INDEX:
               gfx.io.GameDelegate.call("NEW",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.QUIT_INDEX:
               this.ShowConfirmScreen("$Quit to desktop?  Any unsaved progress will be lost.");
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.LOAD_INDEX:
               if(!event.entry.disabled)
               {
                  this.SaveLoadListHolder.__set__isSaving(false);
                  gfx.io.GameDelegate.call("LOAD",[this.SaveLoadListHolder.List_mc.entryList,this.SaveLoadListHolder.__get__batchSize()]);
               }
               else
               {
                  gfx.io.GameDelegate.call("OnDisabledLoadPress",[]);
               }
               break;
            case StartMenu.DLC_INDEX:
               this.StartState(StartMenu.DLC_STATE);
               break;
            case StartMenu.CREDITS_INDEX:
               this.FadeOutAndCall("OpenCreditsMenu");
               break;
            default:
               gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
         }
      }
   }
   function onMainListPress(event)
   {
      this.onCancelPress();
   }
   function onPCQuitButtonPress(event)
   {
      if(event.index == 0)
      {
         gfx.io.GameDelegate.call("QuitToMainMenu",[]);
      }
      else if(event.index == 1)
      {
         gfx.io.GameDelegate.call("QuitToDesktop",[]);
      }
   }
   function onSaveLoadListPress()
   {
      this.onAcceptPress();
   }
   function onMainListMoveUp(event)
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      if(event.scrollChanged == true)
      {
         this.MainList._parent.gotoAndPlay("moveUp");
      }
   }
   function onMainListMoveDown(event)
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      if(event.scrollChanged == true)
      {
         this.MainList._parent.gotoAndPlay("moveDown");
      }
   }
   function onMainListMouseSelectionChange(event)
   {
      if(event.keyboardOrMouse == 0 && event.index != -1)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      }
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this.ButtonRect.AcceptGamepadButton._visible = aiPlatform != 0;
      this.ButtonRect.CancelGamepadButton._visible = aiPlatform != 0;
      this.ButtonRect.AcceptMouseButton._visible = aiPlatform == 0;
      this.ButtonRect.CancelMouseButton._visible = aiPlatform == 0;
      this.DeleteSaveButton.SetPlatform(aiPlatform,abPS3Switch);
      this.MarketplaceButton.SetPlatform(aiPlatform,abPS3Switch);
      this.MainListHolder.SelectionArrow._visible = aiPlatform != 0;
      if(aiPlatform != 0)
      {
         this.ButtonRect.AcceptGamepadButton.SetPlatform(aiPlatform,abPS3Switch);
         this.ButtonRect.CancelGamepadButton.SetPlatform(aiPlatform,abPS3Switch);
      }
      this.MarketplaceButton._visible = aiPlatform == 2;
      this.iPlatform = aiPlatform;
      this.SaveLoadListHolder.__set__platform(this.iPlatform);
      this.MainList.SetPlatform(aiPlatform,abPS3Switch);
   }
   function DoFadeOutMenu()
   {
      this.FadeOutAndCall();
   }
   function DoFadeInMenu()
   {
      this._parent.gotoAndPlay("fadeIn");
      this.EndState();
   }
   function FadeOutAndCall(strCallback, paramList)
   {
      this.strFadeOutCallback = strCallback;
      this.fadeOutParams = paramList;
      this._parent.gotoAndPlay("fadeOut");
      gfx.io.GameDelegate.call("fadeOutStarted",[]);
   }
   function onFadeOutCompletion()
   {
      if(this.strFadeOutCallback != undefined && this.strFadeOutCallback.length > 0)
      {
         if(this.fadeOutParams != undefined)
         {
            gfx.io.GameDelegate.call(this.strFadeOutCallback,this.fadeOutParams);
         }
         else
         {
            gfx.io.GameDelegate.call(this.strFadeOutCallback,[]);
         }
      }
   }
   function StartState(strStateName)
   {
      if(strStateName == StartMenu.SAVE_LOAD_STATE)
      {
         this.ShowDeleteButtonHelp(true);
      }
      else if(strStateName == StartMenu.DLC_STATE)
      {
         this.ShowMarketplaceButtonHelp(true);
      }
      if(this.strCurrentState == StartMenu.MAIN_STATE)
      {
         this.MainList.__set__disableSelection(true);
      }
      this.strCurrentState = strStateName + "StartAnim";
      this.gotoAndPlay(this.strCurrentState);
      gfx.managers.FocusHandler.__get__instance().setFocus(this,0);
   }
   function EndState()
   {
      if(this.strCurrentState == StartMenu.SAVE_LOAD_STATE)
      {
         this.ShowDeleteButtonHelp(false);
      }
      else if(this.strCurrentState == StartMenu.DLC_STATE)
      {
         this.ShowMarketplaceButtonHelp(false);
      }
      if(this.strCurrentState != StartMenu.MAIN_STATE)
      {
         this.strCurrentState = this.strCurrentState + "EndAnim";
         this.gotoAndPlay(this.strCurrentState);
      }
   }
   function ChangeStateFocus(strNewState)
   {
      switch(strNewState)
      {
         case StartMenu.MAIN_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.MainList,0);
            break;
         case StartMenu.SAVE_LOAD_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.SaveLoadListHolder.List_mc,0);
            this.SaveLoadListHolder.List_mc.disableSelection = false;
            break;
         case StartMenu.DLC_STATE:
            this.iLoadDLCListTimerID = setInterval(this,"DoLoadDLCList",500);
            gfx.managers.FocusHandler.__get__instance().setFocus(this.DLCList_mc,0);
            break;
         case StartMenu.MAIN_CONFIRM_STATE:
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
         case StartMenu.PRESS_START_STATE:
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.ButtonRect,0);
      }
   }
   function ShowConfirmScreen(astrConfirmText)
   {
      this.ConfirmPanel_mc.textField.SetText(astrConfirmText);
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.MAIN_CONFIRM_STATE);
   }
   function OnSaveListOpenSuccess()
   {
      if(this.SaveLoadListHolder.__get__numSaves() > 0)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
         this.StartState(StartMenu.SAVE_LOAD_STATE);
      }
      else
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
      }
   }
   function onSaveHighlight(event)
   {
      this.DeleteSaveButton._alpha = event.index != -1?100:50;
      if(this.iPlatform == 0)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      }
   }
   function ConfirmLoadGame(event)
   {
      this.SaveLoadListHolder.List_mc.disableSelection = true;
      this.SaveLoadConfirmText.textField.SetText("$Load this game?");
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.SAVE_LOAD_CONFIRM_STATE);
   }
   function ConfirmDeleteSave()
   {
      this.SaveLoadListHolder.List_mc.disableSelection = true;
      this.SaveLoadConfirmText.textField.SetText("$Delete this save?");
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.DELETE_SAVE_CONFIRM_STATE);
   }
   function ShowDeleteButtonHelp(abFlag)
   {
      this.DeleteSaveButton._visible = abFlag;
      this.VersionText._visible = !abFlag;
   }
   function ShowMarketplaceButtonHelp(abFlag)
   {
      this.MarketplaceButton._visible = abFlag;
      this.VersionText._visible = !abFlag;
   }
   function onProfileChange()
   {
      this.ShowDeleteButtonHelp(false);
      if(this.strCurrentState != StartMenu.MAIN_STATE && this.strCurrentState != StartMenu.PRESS_START_STATE)
      {
         this.StartState(StartMenu.MAIN_STATE);
      }
   }
   function StartLoadingDLC()
   {
      this.LoadingContentMessage.gotoAndPlay("startFadeIn");
      clearInterval(this.iLoadDLCContentMessageTimerID);
      this.iLoadDLCContentMessageTimerID = setInterval(this,"onLoadingDLCMessageFadeCompletion",1000);
   }
   function onLoadingDLCMessageFadeCompletion()
   {
      clearInterval(this.iLoadDLCContentMessageTimerID);
      gfx.io.GameDelegate.call("DoLoadDLCPlugins",[]);
   }
   function DoneLoadingDLC()
   {
      this.LoadingContentMessage.gotoAndPlay("startFadeOut");
   }
   function DoLoadDLCList()
   {
      clearInterval(this.iLoadDLCListTimerID);
      this.DLCList_mc.__get__entryList().splice(0,this.DLCList_mc.__get__entryList().length);
      gfx.io.GameDelegate.call("LoadDLC",[this.DLCList_mc.__get__entryList()],this,"UpdateDLCPanel");
   }
   function UpdateDLCPanel(abMarketplaceAvail, abNewDLCAvail)
   {
      if(this.DLCList_mc.__get__entryList().length > 0)
      {
         this.DLCList_mc._visible = true;
         this.DLCPanel.warningText.SetText(" ");
         if(this.iPlatform != 0)
         {
            this.DLCList_mc.__set__selectedIndex(0);
         }
         this.DLCList_mc.InvalidateData();
      }
      else
      {
         this.DLCList_mc._visible = false;
         this.DLCPanel.warningText.SetText("$No content downloaded");
      }
      this.MarketplaceButton._alpha = !abMarketplaceAvail?50:100;
      if(abNewDLCAvail == true)
      {
         this.DLCPanel.NewContentAvail.SetText("$New content available");
      }
   }
}
