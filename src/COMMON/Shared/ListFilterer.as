class Shared.ListFilterer
{
   function ListFilterer()
   {
      this.iItemFilter = 4294967295;
      this.EntryMatchesFunc = this.EntryMatchesFilter;
      gfx.events.EventDispatcher.initialize(this);
   }
   function __get__itemFilter()
   {
      return this.iItemFilter;
   }
   function __set__itemFilter(aiNewFilter)
   {
      var _loc2_ = this.iItemFilter != aiNewFilter;
      this.iItemFilter = aiNewFilter;
      if(_loc2_ == true)
      {
         this.dispatchEvent({type:"filterChange"});
      }
      return this.__get__itemFilter();
   }
   function __get__filterArray()
   {
      return this._filterArray;
   }
   function __set__filterArray(aNewArray)
   {
      this._filterArray = aNewArray;
      return this.__get__filterArray();
   }
   function SetPartitionedFilterMode(abPartition)
   {
      this.EntryMatchesFunc = !abPartition?this.EntryMatchesFilter:this.EntryMatchesPartitionedFilter;
   }
   function EntryMatchesFilter(aEntry)
   {
      return aEntry != undefined && (aEntry.filterFlag == undefined || (aEntry.filterFlag & this.iItemFilter) != 0);
   }
   function EntryMatchesPartitionedFilter(aEntry)
   {
      var _loc3_ = false;
      if(aEntry != undefined)
      {
         if(this.iItemFilter == 4294967295)
         {
            _loc3_ = true;
         }
         else
         {
            var _loc2_ = aEntry.filterFlag;
            var _loc4_ = _loc2_ & 255;
            var _loc7_ = (_loc2_ & 65280) >>> 8;
            var _loc6_ = (_loc2_ & 16711680) >>> 16;
            var _loc5_ = (_loc2_ & 4278190080) >>> 24;
            _loc3_ = _loc4_ == this.iItemFilter || _loc7_ == this.iItemFilter || _loc6_ == this.iItemFilter || _loc5_ == this.iItemFilter;
         }
      }
      return _loc3_;
   }
   function GetPrevFilterMatch(aiStartIndex)
   {
      var _loc3_ = undefined;
      if(aiStartIndex != undefined)
      {
         var _loc2_ = aiStartIndex - 1;
         while(_loc2_ >= 0 && _loc3_ == undefined)
         {
            if(this.EntryMatchesFunc(this._filterArray[_loc2_]))
            {
               _loc3_ = _loc2_;
            }
            _loc2_ = _loc2_ - 1;
         }
      }
      return _loc3_;
   }
   function GetNextFilterMatch(aiStartIndex)
   {
      var _loc3_ = undefined;
      if(aiStartIndex != undefined)
      {
         var _loc2_ = aiStartIndex + 1;
         while(_loc2_ < this._filterArray.length && _loc3_ == undefined)
         {
            if(this.EntryMatchesFunc(this._filterArray[_loc2_]))
            {
               _loc3_ = _loc2_;
            }
            _loc2_ = _loc2_ + 1;
         }
      }
      return _loc3_;
   }
   function ClampIndex(aiStartIndex)
   {
      var _loc2_ = aiStartIndex;
      if(aiStartIndex != undefined && !this.EntryMatchesFunc(this._filterArray[_loc2_]))
      {
         var _loc3_ = this.GetNextFilterMatch(_loc2_);
         if(_loc3_ == undefined)
         {
            _loc3_ = this.GetPrevFilterMatch(_loc2_);
         }
         if(_loc3_ == undefined)
         {
            _loc3_ = -1;
         }
         _loc2_ = _loc3_;
      }
      return _loc2_;
   }
}
