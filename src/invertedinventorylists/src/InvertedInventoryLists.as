class InvertedInventoryLists extends InventoryLists
{
   function InvertedInventoryLists()
   {
      super();
      this.strHideItemsCode = gfx.ui.NavigationCode.RIGHT;
      this.strShowItemsCode = gfx.ui.NavigationCode.LEFT;
   }
}
