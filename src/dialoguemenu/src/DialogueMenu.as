class DialogueMenu extends MovieClip
{
   static var ALLOW_PROGRESS_DELAY = 750;
   static var SHOW_GREETING = 0;
   static var TOPIC_LIST_SHOWN = 1;
   static var TOPIC_CLICKED = 2;
   static var TRANSITIONING = 3;
   static var iMouseDownExecutionCount = 0;
   function DialogueMenu()
   {
      super();
      this.TopicListHolder = this.TopicListHolder;
      this.TopicList = this.TopicListHolder.List_mc;
      this.SubtitleText = this.SubtitleText;
      this.ExitButton = this.ExitButton;
      this.eMenuState = DialogueMenu.SHOW_GREETING;
      this.bFadedIn = true;
      this.bAllowProgress = false;
   }
   function InitExtensions()
   {
      Mouse.addListener(this);
      gfx.io.GameDelegate.addCallBack("Cancel",this,"onCancelPress");
      gfx.io.GameDelegate.addCallBack("ShowDialogueText",this,"ShowDialogueText");
      gfx.io.GameDelegate.addCallBack("HideDialogueText",this,"HideDialogueText");
      gfx.io.GameDelegate.addCallBack("PopulateDialogueList",this,"PopulateDialogueLists");
      gfx.io.GameDelegate.addCallBack("ShowDialogueList",this,"DoShowDialogueList");
      gfx.io.GameDelegate.addCallBack("StartHideMenu",this,"StartHideMenu");
      gfx.io.GameDelegate.addCallBack("SetSpeakerName",this,"SetSpeakerName");
      gfx.io.GameDelegate.addCallBack("NotifyVoiceReady",this,"OnVoiceReady");
      gfx.io.GameDelegate.addCallBack("AdjustForPALSD",this,"AdjustForPALSD");
      this.TopicList.addEventListener("listMovedUp",this,"playListUpAnim");
      this.TopicList.addEventListener("listMovedDown",this,"playListDownAnim");
      this.TopicList.addEventListener("itemPress",this,"onItemSelect");
      Shared.GlobalFunc.SetLockFunction();
      this.ExitButton.Lock("BR");
      this.ExitButton._x = this.ExitButton._x - 50;
      this.ExitButton._y = this.ExitButton._y - 30;
      this.ExitButton.addEventListener("click",this,"onCancelPress");
      this.TopicListHolder._visible = false;
      this.TopicListHolder.TextCopy_mc._visible = false;
      this.TopicListHolder.TextCopy_mc.textField.textColor = 6316128;
      this.TopicListHolder.TextCopy_mc.textField.verticalAutoSize = "top";
      this.TopicListHolder.PanelCopy_mc._visible = false;
      gfx.managers.FocusHandler.__get__instance().setFocus(this.TopicList,0);
      this.SubtitleText.verticalAutoSize = "top";
      this.SubtitleText.SetText(" ");
      this.SpeakerName.verticalAutoSize = "top";
      this.SpeakerName.SetText(" ");
   }
   function AdjustForPALSD()
   {
      _root.DialogueMenu_mc._x = _root.DialogueMenu_mc._x - 35;
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this.ExitButton.SetPlatform(aiPlatform,abPS3Switch);
      this.TopicList.SetPlatform(aiPlatform,abPS3Switch);
   }
   function SetSpeakerName(strName)
   {
      this.SpeakerName.SetText(strName);
   }
   function handleInput(details, pathToFocus)
   {
      if(this.bFadedIn && Shared.GlobalFunc.IsKeyPressed(details))
      {
         if(details.navEquivalent == gfx.ui.NavigationCode.TAB)
         {
            this.onCancelPress();
         }
         else if(details.navEquivalent != gfx.ui.NavigationCode.UP && details.navEquivalent != gfx.ui.NavigationCode.DOWN || this.eMenuState == DialogueMenu.TOPIC_LIST_SHOWN)
         {
            pathToFocus[0].handleInput(details,pathToFocus.slice(1));
         }
      }
      return true;
   }
   function __get__menuState()
   {
      return this.eMenuState;
   }
   function __set__menuState(aNewState)
   {
      this.eMenuState = aNewState;
      return this.__get__menuState();
   }
   function ShowDialogueText(astrText)
   {
      this.SubtitleText.SetText(astrText);
   }
   function OnVoiceReady()
   {
      this.StartProgressTimer();
   }
   function StartProgressTimer()
   {
      this.bAllowProgress = false;
      clearInterval(this.iAllowProgressTimerID);
      this.iAllowProgressTimerID = setInterval(this,"SetAllowProgress",DialogueMenu.ALLOW_PROGRESS_DELAY);
   }
   function HideDialogueText()
   {
      this.SubtitleText.SetText(" ");
   }
   function SetAllowProgress()
   {
      clearInterval(this.iAllowProgressTimerID);
      this.bAllowProgress = true;
   }
   function PopulateDialogueLists()
   {
      var _loc9_ = 0;
      var _loc11_ = 1;
      var _loc8_ = 2;
      var _loc10_ = 3;
      this.TopicList.ClearList();
      var _loc3_ = 0;
      while(_loc3_ < arguments.length - 1)
      {
         var _loc4_ = {text:arguments[_loc3_ + _loc9_],topicIsNew:arguments[_loc3_ + _loc11_],topicIndex:arguments[_loc3_ + _loc8_]};
         this.TopicList.__get__entryList().push(_loc4_);
         _loc3_ = _loc3_ + _loc10_;
      }
      if(arguments[arguments.length - 1] != -1)
      {
         this.TopicList.SetSelectedTopic(arguments[arguments.length - 1]);
      }
      this.TopicList.InvalidateData();
   }
   function DoShowDialogueList(abNewList, abHideExitButton)
   {
      if(this.eMenuState == DialogueMenu.TOPIC_CLICKED || this.eMenuState == DialogueMenu.SHOW_GREETING && this.TopicList.__get__entryList().length > 0)
      {
         this.ShowDialogueList(abNewList,abNewList && this.eMenuState == DialogueMenu.TOPIC_CLICKED);
      }
      this.ExitButton._visible = !abHideExitButton;
   }
   function ShowDialogueList(abSlideAnim, abCopyVisible)
   {
      this.TopicListHolder._visible = true;
      this.TopicListHolder.gotoAndPlay(!abSlideAnim?"fadeListIn":"slideListIn");
      this.eMenuState = DialogueMenu.TRANSITIONING;
      this.TopicListHolder.TextCopy_mc._visible = abCopyVisible;
      this.TopicListHolder.PanelCopy_mc._visible = abCopyVisible;
   }
   function onItemSelect(event)
   {
      if(this.bAllowProgress && event.keyboardOrMouse != 0)
      {
         if(this.eMenuState == DialogueMenu.TOPIC_LIST_SHOWN)
         {
            this.onSelectionClick();
         }
         else if(this.eMenuState == DialogueMenu.TOPIC_CLICKED || this.eMenuState == DialogueMenu.SHOW_GREETING)
         {
            this.SkipText();
         }
         this.bAllowProgress = false;
      }
   }
   function SkipText()
   {
      if(this.bAllowProgress)
      {
         gfx.io.GameDelegate.call("SkipText",[]);
         this.bAllowProgress = false;
      }
   }
   function onMouseDown()
   {
      DialogueMenu.iMouseDownExecutionCount = DialogueMenu.iMouseDownExecutionCount + 1;
      if(DialogueMenu.iMouseDownExecutionCount % 2 == 0)
      {
         return undefined;
      }
      this.onItemSelect();
   }
   function onCancelPress()
   {
      if(this.eMenuState == DialogueMenu.SHOW_GREETING)
      {
         this.SkipText();
      }
      else
      {
         this.StartHideMenu();
      }
   }
   function StartHideMenu()
   {
      this.SubtitleText._visible = false;
      this.bFadedIn = false;
      this.SpeakerName.SetText(" ");
      this.ExitButton._visible = false;
      this._parent.gotoAndPlay("startFadeOut");
      gfx.io.GameDelegate.call("CloseMenu",[]);
   }
   function playListUpAnim(aEvent)
   {
      if(aEvent.scrollChanged == true)
      {
         aEvent.target._parent.gotoAndPlay("moveUp");
      }
   }
   function playListDownAnim(aEvent)
   {
      if(aEvent.scrollChanged == true)
      {
         aEvent.target._parent.gotoAndPlay("moveDown");
      }
   }
   function onSelectionClick()
   {
      if(this.eMenuState == DialogueMenu.TOPIC_LIST_SHOWN)
      {
         this.eMenuState = DialogueMenu.TOPIC_CLICKED;
      }
      if(this.TopicList.__get__scrollPosition() != this.TopicList.__get__selectedIndex())
      {
         this.TopicList.RestoreScrollPosition(this.TopicList.__get__selectedIndex(),true);
         this.TopicList.UpdateList();
      }
      this.TopicListHolder.gotoAndPlay("topicClicked");
      this.TopicListHolder.TextCopy_mc._visible = true;
      this.TopicListHolder.TextCopy_mc.textField.SetText(this.TopicListHolder.List_mc.selectedEntry.text);
      var _loc2_ = this.TopicListHolder.TextCopy_mc._y - this.TopicListHolder.List_mc._y - this.TopicListHolder.List_mc.Entry4._y;
      this.TopicListHolder.TextCopy_mc.textField._y = 6.25 - _loc2_;
      gfx.io.GameDelegate.call("TopicClicked",[this.TopicList.__get__selectedEntry().topicIndex]);
   }
   function onFadeOutCompletion()
   {
      gfx.io.GameDelegate.call("FadeDone",[]);
   }
}
