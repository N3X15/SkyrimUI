# Fresh Decompile

This is here to document the process so you don't have to reinvent the wheel like I did.

**You should not do this yourself.  The code in here has been edited and documented to clarify its purpose.**

## Requirements
* Adobe Flash CS6 Professional
  * **TODO:** Work on making this compatible with FlashDevelop
* Python 2.7 (http://python.org)
  * Run `pip install lxml twisted psutil pyyaml jinja2 toml requests six` after installing.
* JPEXS Flash Decompiler (FFDEC) >= 9.0.0

## Process
0. Rename `user-config.yml.example` to `user-config.yml`.
1. Run `git submodule update --init --recursive --remote` to pull in dependencies.
2. Make a backup of `src/`.
3. Export `Skyrim - Interface.bsa` to `exported/`.
4. Run `python scripts/decompile-sse.py` - **THIS WILL DELETE AND REGENERATE ALL CODE AND PROJECTS IN THE `src` DIRECTORY.**
5. Compare `src/` against your backup of `src/` with WinMerge or a similar diffing tool and reimport all the comments and crap you broke.
